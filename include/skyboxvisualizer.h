#ifndef SKYBOXVISUALIZER_H
#define SKYBOXVISUALIZER_H

#include "shaderloader.h"
#include "glm/glm.hpp"

#include <gmParametricsModule>

// qt
#include <QImage>
#include <QOpenGLTexture>
#include <QImageReader>
#include <QDirIterator>

namespace ADJlib {

/*! \class SkyboxVisualizer
 *
 * Visulizer for skybox rendering. Uses shaders to display and texture the skybox.
 * Extends the built-in parametric surface visualizer of GMlib, with some
 * elements from the texture visualizer. Creates a cube map from skybox textures
 * using standard openGL calls, not the built-in GMlib texture2D functionality.
 * Based on skybox from STE6249 and tutorial from
 * http://learnopengl.com/#!Advanced-OpenGL/Cubemaps (11/10-16)
 *
 * Andreas Dyrøy Jansson 2016
 */
class SkyboxVisualizer : public GMlib::PSurfVisualizer<float, 3> {
    GM_VISUALIZER(SkyboxVisualizer)

public:
    SkyboxVisualizer();
    SkyboxVisualizer(const SkyboxVisualizer& copy);

    void render(const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer) const;
    void renderGeometry(const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color) const;

private:
    GMlib::GL::VertexBufferObject _vertexBuffer;
    std::vector<glm::vec3> _vertexArray;

    GLuint _texId;
    GMlib::GL::Program _prog;
    GMlib::GL::Program _color_prog;

    void loadSkyboxTextures();
};
}

#endif // SKYBOXVISUALIZER_H
