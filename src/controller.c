/*! \file controller.c
 *
 *  Implementation of the controller template class.
 */

#include "../include/controller.h"
#include "../scenario.h"
namespace ADJlib {

template <typename T>
Controller<T>::Controller(Scenario *scenario, int mode) {

    _scenario = scenario;

    this->setSurroundingSphere(GMlib::Sphere<float, 3>(100.0f));

    _grav_const = UNIVERSAL_GRAVITY_CONSTANT;

    _dt_multiplier = 1;
    _dt_sum = 0;

    _yaw = 0;
    _thrust = 0;
    _pitch = 0;
    _roll = 0;
    _warp_factor = 0;

    switch (mode) {
    case Game :
        initGame();
        break;
    case Mode::Simulation :
        initSimulation();
        break;
    default : std::cerr << "Invalid option '" << mode << "'" << std::endl;
        return;
    };

    _mode = mode;

    updateGuiScalingLabel();
}


template <typename T>
void Controller<T>::initGame() {

    _distance_scaling_pow = GAME_DISTANCE_SCALING;
    _size_scaling = GAME_SIZE_SCALING;
    std::cout << "Starting game..." << std::endl;
}

template <typename T>
void Controller<T>::initSimulation() {

    _distance_scaling_pow = SIMULATOR_DISTANCE_SCALING;
    _size_scaling = SIMULATOR_SIZE_SCALING;
    std::cout << "Starting simulation..." << std::endl;
    emit _scenario->signUpdateShipStats(QString("Simulating the Solar System"));
}


template <typename T>
Controller<T>::~Controller() {

    _objects.clear();
    _despawnable_objects.clear();
}


/**
 * Computes the gravitational force between two objects in space
 */
template <typename T>
GMlib::Vector<T, 3> Controller<T>::computeForce(std::shared_ptr<SpaceObject<T>> o1, std::shared_ptr<SpaceObject<T>> o2) const {

    const auto pos1 = o1->getGlobalPos();
    const auto pos2 = o2->getGlobalPos();

    GMlib::Vector<T, 3> r = makeTVector(pos1 - pos2);

    // G * (m1 * m2) / (r^2)
    T force =  scaleDownPhysics(_grav_const) * (o1->getMass() * o2->getMass())
            / ((r) * (r));

    r.setLength(1);

    GMlib::Vector<T, 3> forceVector  = r * scaleDownPhysics(force);

    return forceVector;
}


/**
 * Checks for collisions between all objects in space
 */
template <typename T>
void Controller<T>::collisionDetect(T dt) {

    std::vector<CollisionObject<T>> coll;
    for (int i = 0; i < _objects.size(); i++) {
        _objects[i]->setStep(_objects[i]->calculateStep(dt));
        for (int j = i + 1; j < _objects.size(); j++) {
            _objects[j]->setStep(_objects[j]->calculateStep(dt));
            CollisionObject<T> obj;

            if (findCollision(_objects[i], _objects[j], dt, T(0), obj)) {
                coll.push_back(obj);
            }

        }
    }


    // Handle all other collisions
    while (coll.size() > 0) {
        std::sort(coll.begin(), coll.end());
        std::unique(coll.begin(), coll.end());
        CollisionObject<T> cob = coll.front();
        coll.erase(coll.begin());

        auto object1 = cob.getObject1();
        auto object2 = cob.getObject2();

        // Handle nullptr's
        if(object1 != nullptr && object2 != nullptr) {

            object1->setVelocity(cob.getNewV1());
            object1->translateGlobal(makeFloatVector(object1->getStep() * cob.getX()));

            object2->setVelocity(cob.getNewV2());
            object2->translateGlobal(makeFloatVector(object2->getStep() * cob.getX()));

            for (int i = 0; i < _objects.size(); i++) {
                CollisionObject<T> obj;
                if (object1 != _objects[i] && object2 != _objects[i]) {
                    if (findCollision(object1, _objects[i], dt, cob.getX(), obj)) {
                        coll.push_back(obj);
                    }
                    if (findCollision(object2, _objects[i], dt, cob.getX(), obj)) {
                        coll.push_back(obj);
                    }
                }
            }
        }

    }
} // END collisionDetect


/**
 * Finds collision between two objects using formula
 */
template <typename T>
bool Controller<T>::findCollision(std::shared_ptr<SpaceObject<T>> p1, std::shared_ptr<SpaceObject<T>> p2, T dt, T oldX, CollisionObject<T> &obj) {

    auto ds1 = p1->getStep();
    auto ds2 = p2->getStep();

    const auto pos1 = p1->getGlobalPos();
    const auto pos2 = p2->getGlobalPos();

    GMlib::Vector<T, 3> dp = makeTVector(pos1 - pos2);

    GMlib::Vector<T, 3> ds = ds1 - ds2;

    T r = p1->getRadius() + p2->getRadius();

    T a = ds * ds;
    T b = 2 * (ds * dp);
    T c = (dp * dp) - (r * r);

    T sumUnderSqrt = ((b * b) - 4 * a * c);

    if (sumUnderSqrt > 0) {
        T x = (-b - std::sqrt(sumUnderSqrt)) / (2 * a);

        if (x > oldX && x <= 1) {
            obj.make(p1, p2, x, dp);
            return true;
        }
    }

    return false;

}


/**
 * Helper functions to scale values used for computaion and plotting separately
 */
template <typename T>
T Controller<T>::scaleDownPhysics(T value) const {

    return value * std::pow(10, -_distance_scaling_pow);
}


template <typename T>
T Controller<T>::scaleUpPhysics(T value) const {

    return value * std::pow(10, _distance_scaling_pow);
}


template <typename T>
T Controller<T>::scaleVisual(T size) const {

    return size * _size_scaling;
}


/**
 * Emits a signal to update scaling label in ui
 */
template <typename T>
void Controller<T>::updateGuiScalingLabel() const {

    std::cout << "Running simulation at " << _dt_multiplier << "x speed" << std::endl;
    emit _scenario->signUpdateSimTimeScaling(QString("Running simulation at " + QString::number(_dt_multiplier, 'f', 0) + "x speed"));
}


/**
 * Helper functions to convert a vector from float to double and vice versa
 */
template <typename T>
GMlib::Vector<T, 3> Controller<T>::makeTVector(GMlib::Vector<float, 3> vector) const {

    return GMlib::Vector<T, 3>(vector(0), vector(1), vector(2));
}


template <typename T>
GMlib::Vector<float, 3> Controller<T>::makeFloatVector(GMlib::Vector<T, 3> vector) const {

    return GMlib::Vector<float, 3>(vector(0), vector(1), vector(2));
}


/**
 * Increases the speed in which the simulation is run
 */
template <typename T>
void Controller<T>::increaseSimSpeed() {

    if (_warp_factor == 0) {

        if (_dt_multiplier == 1)
            _dt_multiplier = MIN_DT_SCALING;

        else if (_dt_multiplier * 2 <= MAX_DT_SCALING)
            _dt_multiplier *= 2;

        else
            std::cout<<"Max scaling reached!"<<std::endl;

        updateGuiScalingLabel();
    }
    else {
        std::cout<<"Can't scale time while at warp!"<<std::endl;
    }


}


/**
 * Decreases the speed in which the simulation is run
 */
template <typename T>
void Controller<T>::decreaseSimSpeed() {

    if (_dt_multiplier == MIN_DT_SCALING)
        _dt_multiplier = 1;

    else if (_dt_multiplier / 2 >= MIN_DT_SCALING)
        _dt_multiplier /= 2;

    else
        std::cout<<"Running in real time!"<<std::endl;

    updateGuiScalingLabel();

}


/**
 * Starship controls
 */
template <typename T>
void Controller<T>::addThrustStarship(int direction) {

    if ((direction < 0) != (_thrust < 0) || std::abs(_thrust) < 1)
        _thrust += direction * 0.0001;
    else
        std::cout<<"Max thrust reached!"<<std::endl;

    if (direction == 0)
        _thrust = direction;

}


template <typename T>
void Controller<T>::yawStarship(int direction) {

    _yaw = direction;

}


template <typename T>
void Controller<T>::pitchStarship(int direction) {

    _pitch = direction;
}


template <typename T>
void Controller<T>::rollStarship(int direction) {

    _roll = direction;
}


template <typename T>
void Controller<T>::engageWarp(int warpFactor) {

    if (warpFactor != 0) {

        // Can't scale time while warping
        if (_dt_multiplier != 1) {
            _dt_multiplier = 1;

            updateGuiScalingLabel();
        }

        std::cout<<"Warp "<<warpFactor<<" engaged!"<<std::endl;

        auto floatVelocity = _starship->getDir() * scaleDownPhysics(SPEED_OF_LIGHT) * std::pow(3, warpFactor - 1);
        _starship->setVelocity(GMlib::Vector<T, 3>(makeTVector(floatVelocity)));
    }
    else {
        std::cout<<"Dropping from warp!"<<std::endl;

        _starship->setVelocity(GMlib::Vector<T, 3>(0, 0, 0));
        _thrust = 0;
    }

    _warp_factor = warpFactor;

}


template <typename T>
void Controller<T>::fireTorpedo() {

    GMlib::Vector<float, 3> dir = _starship->getDir() * scaleDownPhysics(SPEED_OF_LIGHT / 10.f) + _starship->getVelocity() ;

    auto torpedo = std::make_shared<SpaceObject<T>>(scaleVisual(1e5),
                                                    scaleDownPhysics(100.f),
                                                    makeTVector(dir));

    torpedo->translateGlobal(GMlib::Vector<float, 3>(_starship->getGlobalPos()));
    torpedo->translateGlobal(GMlib::Vector<float, 3>(_starship->getRadius() * _starship->getDir()));
    torpedo->toggleDefaultVisualizer();
    torpedo->replot(10, 10, 1, 1);
    torpedo->setMaterial(GMlib::GMmaterial::ruby());
    torpedo->setDespawnTime(60.f); // Torpedos "explode" (despawn) after 60 seconds
    insert(torpedo.get());
    _objects.push_back(torpedo);
    _despawnable_objects.push_back(torpedo);

}


template <typename T>
void Controller<T>::localSimulate(double dt) {

    if (!_despawnable_objects.empty())
        cleanUpDespawnableObjects();

    for (int i = 0; i < _objects.size() - 1; i++) {
        for (int j = i + 1; j < _objects.size(); j++) {

            GMlib::Vector<T, 3> newForce = this->computeForce(_objects[i], _objects[j]);
            _objects[i]->addForce(-newForce);
            _objects[j]->addForce(newForce);

        }
    }

    // Starship controls
    if (_starship != nullptr) {

        if (_yaw != 0) {
            _starship->turn(GMlib::Angle(_yaw));
        }

        if (_pitch != 0) {
            _starship->tilt(GMlib::Angle(_pitch));
        }

        if (_roll != 0) {
            _starship->roll(GMlib::Angle(_roll));
        }

        if (_thrust != 0) {

            auto floatThrust = _thrust * _starship->getDir();
            _starship->addForce(makeTVector(floatThrust));
        }

        const GMlib::Vector<float, 3> transVec = makeFloatVector(_starship->getStep());
        _cam->translateGlobal(transVec);
        //_starship->translateGlobal(transVec);
        _camSphere->translateGlobal(transVec);

        //std::cout << "cam pos: " << _cam->getGlobalPos() << std::endl;
        //std::cout << "ship pos: " << _starship->getGlobalPos() << std::endl;


        _dt_sum += dt;

        // Update ui label 10 times pr. second
        if (_dt_sum >= 0.1) {

            auto velocity =  scaleUpPhysics(_starship->getVelocity().getLength());
            int yawAngle = std::abs(std::atan2(_starship->getDir()(1), _starship->getDir()(0)) * (180.f / M_PI));
            int pitchAngle = std::atan2(_starship->getUp()(1), _starship->getUp()(2)) * (180.f / M_PI);
            int rollAngle = std::atan2(_starship->getUp()(0), _starship->getUp()(2)) * (180.f / M_PI);


            QString shipStats = "";

            shipStats += _warp_factor != 0 ? "Warp " + QString::number(_warp_factor)
                                           : "Thrust: " + QString::number(scaleUpPhysics(_thrust), 'f', 0) + "N";

            shipStats += velocity > 1000 ? " | Velocity: " + QString::number(velocity / 1000, 'f', 3) + "km/s"
                                         : " | Velocity: " + QString::number(velocity, 'f', 0) + "m/s";


            shipStats += " | Yaw: " + QString::number(yawAngle) + " | Pitch: " + QString::number(-pitchAngle) + " | Roll: " + QString::number(rollAngle);

            emit _scenario->signUpdateShipStats(shipStats);

            _dt_sum = 0;
        }

    }


    // Can't scale time while at warp
    if (_warp_factor == 0)
        dt *= _dt_multiplier;

    collisionDetect(dt);

} // END localsimulate


/**
 * Checks for and removes expired objects
 */
template <typename T>
void Controller<T>::cleanUpDespawnableObjects() {

    for (int i = 0; i < _despawnable_objects.size(); i++) {
        if (_despawnable_objects[i]->isExpired()) {
            remove(_despawnable_objects[i].get());
            _objects.erase(std::find(_objects.begin(), _objects.end(), _despawnable_objects[i]));
            _despawnable_objects.erase(_despawnable_objects.begin() + i);
        }
    }
}


/**
 * Generates a simple model of our solar system with asteroids and kuiper objects.
 * Uses DataObject to insert planet data in a for-loop
 */
template <typename T>
void Controller<T>::makeSolarSystem(std::shared_ptr<GMlib::Camera> cam, GMlib::PointLight *light) {

    _cam = cam;

    TextureLoader loader;
    auto visualizers = loader.loadPlanetTextures();

    if (visualizers.empty()) {
        std::cerr << "Error: No textures loaded!" << std::endl;
        return;
    }

    // The dataVector contains data for all the planets + the Sun and the Moon

    DataLoader<T> dataHelper;
    auto dataVector = dataHelper.getPlanetDataVector();
    std::cout<<"Generating planets..."<<std::endl;
    for (int i = 0; i < dataVector.size(); i++) {

        auto planet = std::make_shared<SpaceObject<T>>(scaleVisual(dataVector[i].getRadius()),
                                                       scaleDownPhysics(dataVector[i].getMass()),
                                                       GMlib::Vector<T,3>(0, scaleDownPhysics(dataVector[i].getVelocity()), 0),
                                                       dataVector[i].getRotation());

        planet->translateGlobal(GMlib::Vector<float, 3>(scaleDownPhysics(dataVector[i].getOrbit()), 0.f, 0.f));
        planet->rotateGlobal(GMlib::Angle(dataVector[i].getTilt()), GMlib::Vector<float, 3>(0.f, 1.f, 0.f));
        planet->insertVisualizer(visualizers[i]);
        planet->replot(70, 70, 1, 1);
        planet->setMaterial(GMlib::GMmaterial::silver());

        insert(planet.get());
        _objects.push_back(planet);

    }

    // Special cases; lights, rings etc

    auto sun = _objects[0];
    if (_mode == Mode::Simulation) {
        sun->setRadius(_objects[0]->getRadius() * 1e-2);
        sun->replot(70, 70, 1, 1);
    }

    sun->insert(light);
    sun->setMaterial(GMlib::GMcolor::white());

    auto saturn = _objects[7];

    float ringSize = saturn->getRadius() * 1.5f;

    auto saturnRing = new GMlib::PTorus<float>(ringSize, ringSize / 4.f, ringSize * 1e-3);
    saturnRing->insertVisualizer(visualizers[11]);
    saturnRing->replot(70, 70, 1, 1);
    saturnRing->setMaterial(GMlib::GMmaterial::silver());
    saturn->insert(saturnRing);

    auto uranus = _objects[8];

    ringSize = uranus->getRadius() * 1.5f;

    auto uranusRing = new GMlib::PTorus<float>(ringSize, ringSize / 4.f, ringSize * 1e-3);
    uranusRing->insertVisualizer(visualizers[12]);
    uranusRing->replot(70, 70, 1, 1);
    uranusRing->setMaterial(GMlib::GMmaterial::silver());
    uranus->insert(uranusRing);


    switch (_mode) {
    case Mode::Game : {
        // Starship setup
        _starship = std::make_shared<SpaceObject<T>>(15.5, scaleDownPhysics(STARSHIP_MASS),
                                                     GMlib::Vector<T, 3>(0, scaleDownPhysics(EARTH_VELOCITY), 0));

        _starship->translateGlobal(GMlib::Vector<float, 3>(float(scaleDownPhysics(EARTH_ORBIT)), float(scaleVisual(EARTH_RADIUS) * -100), 0.f));
        _starship->toggleDefaultVisualizer();
        _starship->replot(7, 7, 1, 1);
        _starship->setVisible(false);

        insert(_starship.get());
        _objects.push_back(_starship);

        //auto starshipModel = new Starship();
        _jjModel = std::make_shared<Model>("jjprise\\", "newEnterprise.obj");

        //starshipModel->translate(GMlib::Vector<float, 3>(9.f, 0.f, 0.f));
        //_starship->insert(starshipModel);

        _starship->insert(_jjModel.get());
        _jjModel->rotate(GMlib::Angle(-180), GMlib::Vector<float, 3>(0, 0, 1));
        //_jjModel->translate(GMlib::Vector<float, 3>(9.f, 0.f, 0.f));
        _jjModel->translate(GMlib::Vector<float, 3>(-8.5, 2.16, -4));
        _jjModel->scale(GMlib::Point<float, 3>(0.12, 0.12, 0.12));
        //_jjModel->translateGlobal(GMlib::Vector<float, 3>(_starship->getGlobalPos()));
        //insert(_jjModel.get());


        _cam->translateGlobal(GMlib::Vector<float, 3>(_starship->getGlobalPos()));
        _cam->rotateGlobal(GMlib::Angle(-90), GMlib::Vector<float, 3>(0.f, 0.f, 1.f));
        _cam->rotateGlobal(GMlib::Angle(10), GMlib::Vector<float, 3>(0.f, 1.f, 0.f));
        _cam->translateGlobal(GMlib::Vector<float, 3> (_starship->getRadius() * -4.f,
                                                       0.f, _starship->getRadius() * 2.f));
        insert(_cam.get());

        _camSphere = std::make_shared<GMlib::PSphere<float>>(20);
        _camSphere->toggleDefaultVisualizer();
        _camSphere->replot(70, 70, 1, 1);
        _camSphere->setVisible(false);
        _camSphere->translateGlobal(_starship->getGlobalPos());
        insert(_camSphere.get());
        _starship->rotateGlobal(GMlib::Angle(90), GMlib::Vector<float, 3>(0.f, 0.f, 1.f));
        _cam->lock(_camSphere.get());

        //std::cout << "model pos: " << jjModel->getGlobalPos() << std::endl;
        //std::cout << "ship pos: " << _starship->getGlobalPos() << std::endl;

    }
        break;
    case Mode::Simulation : {
        // Default cam setup
        _cam->rotateGlobal(GMlib::Angle(-90), GMlib::Vector<float, 3>(1.f, 0.f, 0.f));
        _cam->translateGlobal(GMlib::Vector<float, 3>(0.f, 0.f, _objects[0]->getRadius() * 10));

        _cam->lock(sun.get());
        insert(_cam.get());
    }
        break;
    default : {
        // Earth cam setup
        auto earth = _objects[3];
        _cam->rotateGlobal(GMlib::Angle(-90), GMlib::Vector<float, 3>(1.f, 0.f, 0.f));
        _cam->translateGlobal(GMlib::Vector<float, 3> (0.f, 0.f, earth->getRadius() * 10));
        earth->insert(_cam.get());
    }
    };


    // Random bodies:
    // Asteroids
    generateRandomBodies(50, ASTEROID_ORBIT_MIN, ASTEROID_ORBIT_MAX, ASTEROID_MASS, ASTEROID_RADIUS, ASTEROID_VELOCITY, visualizers[13]);

    // Kuiper belt objects
    generateRandomBodies(20, KUIPER_ORBIT_MIN, KUIPER_ORBIT_MAX, KUIPER_MASS, KUIPER_RADIUS, KUIPER_VELOCITY, visualizers[10]);

    //_skybox = std::make_shared<GMlib::PSphere<float>>(scaleDownPhysics(KUIPER_ORBIT_MAX * 5));
    //_skybox->insertVisualizer(visualizers[14]);
    //_skybox->replot(70, 70, 1, 1);
    //_skybox->setMaterial(GMlib::GMmaterial::silver());

    //insert(_skybox.get());

    std::cout<<"Done!"<<std::endl;

} // END makeSolarSystem


/**
 * Generates random objects in a circle around the Sun
 */
template <typename T>
void Controller<T>::generateRandomBodies(int noOfObjects, float minOrbit, float maxOrbit, T mass, float radius, float velocity, GMlib::PSurfTexVisualizer<float, 3> *visualizer) {

    std::cout<<"Generating random objects..."<<std::endl;

    GMlib::Random<float> random;
    random.setSeed(time(NULL));
    random.set(scaleDownPhysics(minOrbit), scaleDownPhysics(maxOrbit));

    GMlib::Vector<float, 3> rotAxis(0.f, 0.f, 1.f);

    T angle = float(M_2PI) / float(noOfObjects);
    GMlib::HqMatrix<float, 3> rotMat(GMlib::Angle(angle), rotAxis);

    // initial translation and velocity
    GMlib::Vector<float, 3> orbitTranslation(1.f, 0.f, 0.f);
    GMlib::Vector<float, 3> velocityVector(0.f, 1.f, 0.f);

    for (int i = 0; i < noOfObjects; i++) {

        float velocityScaled = scaleDownPhysics(velocity);
        float orbit = random.get();

        velocityVector.setLength(1.f);
        orbitTranslation.setLength(1.f);

        // Rotate
        velocityVector = (rotMat * velocityVector) * velocityScaled;
        orbitTranslation = (rotMat * orbitTranslation) * orbit;

        auto body = std::make_shared<SpaceObject<T>>(scaleVisual(radius), scaleDownPhysics(mass), GMlib::Vector<T, 3>(makeTVector(velocityVector)));
        body->translate(orbitTranslation);
        body->insertVisualizer(visualizer);
        body->replot(30, 30, 1, 1);
        body->setMaterial(GMlib::GMmaterial::chrome());
        insert(body.get());
        _objects.push_back(body);

    }
}
}

