#include "../include/modelvisualizer.h"

namespace ADJlib {

ModelVisualizer::ModelVisualizer() {

    // Load the shaders
    ShaderLoader sLoader;
    _prog = sLoader.loadShader("model");

    _color_prog.acquire("color");
    assert(_color_prog.isValid());
}

ModelVisualizer::ModelVisualizer(const ModelVisualizer &copy) : GMlib::PSurfVisualizer<float, 3>(copy) {

    // Load the shaders
    ShaderLoader sLoader;
    _prog = sLoader.loadShader("model");

    _color_prog.acquire("color");
    assert(_color_prog.isValid());
}

void ModelVisualizer::setModelData(const std::vector<std::shared_ptr<Shape>> shapes) {

    _shapes = shapes;
}

void ModelVisualizer::render(const GMlib::SceneObject *obj, const GMlib::DefaultRenderer *renderer) const {

    const GMlib::Camera* cam = renderer->getCamera();
    const GMlib::HqMatrix<float, 3> &mvmat = obj->getModelViewMatrix(cam);
    const GMlib::HqMatrix<float, 3> &pmat = obj->getProjectionMatrix(cam);

    GMlib::SqMatrix<float,3> nmat = mvmat.getRotationMatrix();

    this->glSetDisplayMode();

    _prog.bind(); {

        // Model view and projection matrices
        _prog.uniform( "u_mvmat", mvmat );
        _prog.uniform( "u_mvpmat", pmat * mvmat );
        _prog.uniform( "u_nmat", nmat );

        _prog.uniform("in_lightPosition", GMlib::Point<float, 3>(2.f, 4.f, 10.f));

        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation("in_vertex");
        GMlib::GL::AttributeLocation norm_loc = _prog.getAttributeLocation("in_normal");
        GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation("in_tex");

        for (auto shape : _shapes) {

            _prog.uniform("in_objColor", shape->getColor()); // Color for phong shader
            _prog.uniform("u_isTextured", shape->isTextured()); // If the shape has a texture
            if (shape->isTextured())
                _prog.uniform("u_tex", shape->getTexture(), static_cast<GLenum>(GL_TEXTURE0), 0);
            shape->render(vert_loc, norm_loc, tex_loc);
        }

    } _prog.unbind();
}

void ModelVisualizer::renderGeometry(const GMlib::SceneObject *obj, const GMlib::Renderer *renderer, const GMlib::Color &color) const {

    _color_prog.bind(); {
        _color_prog.uniform("u_color", color);
        _color_prog.uniform("u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()));

        GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation("in_vertex");

        for (auto shape : _shapes)
            shape->renderGeo(vertice_loc);

    } _color_prog.unbind();
}
}
