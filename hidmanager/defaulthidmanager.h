#ifndef DEFAULTHIDMANAGER_H
#define DEFAULTHIDMANAGER_H

#include "standardhidmanager.h"


#include <queue>

// local
class GMlibWrapper;


class DefaultHidManager : public StandardHidManager {
    Q_OBJECT
public:
    explicit DefaultHidManager( QObject* parent = Q_NULLPTR );
    virtual ~DefaultHidManager();

    static const unsigned int   OGL_TRIGGER {8};

    void                        setupDefaultHidBindings();

    void                        init( GMlibWrapper& gmlib );

public slots:
    void                        triggerOGLActions();

protected:
    void                        triggerAction(const HidAction *action, const HidInputEvent::HidInputParams &params) override;

signals:
    void signToggleSimulation();
    void signOpenCloseHidHelp();
    // controls
    void signControlForward();
    void signControlReverse();
    void signControlRollLeft();
    void signControlRollRight();
    void signControlPitchDown();
    void signControlPitchUp();
    void signControlYawLeft();
    void signControlYawRight();

    void signControlPitchStop();
    void signControlRollStop();
    void signControlYawStop();

    void signControlShoot();
    void signControlEngageWarp(const int value);
    void signIncreaseSimSpeed();
    void signDecreaseSimSpeed();

private slots:
    virtual void                      heDeSelectAllObjects();
    virtual void                      heEdit();
    virtual void                      heLockTo( const HidInputEvent::HidInputParams& params );
    virtual void                      heMoveCamera( const HidInputEvent::HidInputParams& params );
    virtual void                      heMoveSelectedObjects( const HidInputEvent::HidInputParams& params );
    virtual void                      hePanHorizontal( const HidInputEvent::HidInputParams& params );
    virtual void                      hePanVertical( const HidInputEvent::HidInputParams& params );
    virtual void                      heReplotQuick( int factor );
    virtual void                      heReplotQuickHigh();
    virtual void                      heReplotQuickLow();
    virtual void                      heReplotQuickMedium();
    virtual void                      heRotateSelectedObjects( const HidInputEvent::HidInputParams& params );
    virtual void                      heScaleSelectedObjects( const HidInputEvent::HidInputParams& params );
    virtual void                      heSelectAllObjects();
    virtual void                      heSelectObject( const HidInputEvent::HidInputParams& params );
    virtual void                      heSelectObjects( const HidInputEvent::HidInputParams& params );
    virtual void                      heSelectObjectTree( GMlib::SceneObject* obj );
    virtual void                      heToggleObjectDisplayMode();
    virtual void                      heToggleSimulation();
    virtual void                      heToggleSelectAllObjects();
    //  virtual void                      heUnlockCamera();
    virtual void                      heZoom( const HidInputEvent::HidInputParams& params );

    virtual void                      heLeftMouseReleaseStuff();
    virtual void                      heOpenCloseHidHelp();

    // controls
    virtual void                      heControlForward();
    virtual void                      heControlReverse();
    virtual void                      heControlRollLeft();
    virtual void                      heControlRollRight();
    virtual void                      heControlPitchDown();
    virtual void                      heControlPitchUp();
    virtual void                      heControlYawLeft();
    virtual void                      heControlYawRight();

    virtual void                      heControlPitchStop();
    virtual void                      heControlRollStop();
    virtual void                      heControlYawStop();

    virtual void                      heControlShoot();

    virtual void                      heControlWarp0();
    virtual void                      heControlWarp1();
    virtual void                      heControlWarp2();
    virtual void                      heControlWarp3();
    virtual void                      heControlWarp4();
    virtual void                      heControlWarp5();
    virtual void                      heControlWarp6();
    virtual void                      heControlWarp7();
    virtual void                      heControlWarp8();
    virtual void                      heControlWarp9();

    virtual void                      heControlIncreaseSimSpeed();
    virtual void                      heControlDecreaseSimSpeed();

private:
    GMlib::Camera*                    findCamera( const QString& view_name ) const;
    float                             cameraSpeedScale( GMlib::Camera* cam ) const;
    GMlib::Scene*                     scene() const;
    GMlib::SceneObject*               findSceneObject(const QString& view_name, const GMlib::Point<int,2>& pos);

    GMlib::Point<int,2>               toGMlibViewPoint(const QString& view_name, const QPoint& pos);

    GMlibWrapper*                     _gmlib;

    std::queue<std::pair<const HidAction*,HidInputEvent::HidInputParams>>   _ogl_actions;


};

#endif // DEFAULTHIDMANAGER_H
