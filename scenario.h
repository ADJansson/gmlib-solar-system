#ifndef SCENARIO_H
#define SCENARIO_H


#include "application/gmlibwrapper.h"

// qt
#include <QObject>

namespace ADJlib {

class Skybox;

template<typename T>
class Controller;
}


class Scenario : public GMlibWrapper {
    Q_OBJECT
public:
    using GMlibWrapper::GMlibWrapper;

    // Controls
    void controlForward(); // W
    void controlReverse(); // S
    void controlPitchDown(); // up
    void controlPitchUp(); // down
    void controlRollLeft(); // A
    void controlRollRight(); // D
    void controlYawLeft(); // left
    void controlYawRight(); // right

    void controlReleasePitch();
    void controlReleaseRoll();
    void controlReleaseYaw();

    void controlShootPhoton();
    void controlIncreaseSimSpeed(); // +
    void controlDecreaseSimSpeed(); // -

    void    initializeScenario() override;
    void    cleanupScenario() override;

public slots:
    void controlEngageWarp(int factor);

private:
    ADJlib::Controller<double>* _controller;
    std::shared_ptr<ADJlib::Skybox> _skybox;

signals:
    void signUpdateShipStats(const QString& text) const;
    void signUpdateSimTimeScaling(const QString& text);

};

#endif // SCENARIO_H
