/*! \file spaceobject.c
 *
 *  Implementation of the spaceobject template class.
 */

#include "../include/spaceobject.h"
namespace ADJlib {

template <typename T>
SpaceObject<T>::SpaceObject(T size, T mass, GMlib::Vector<T, 3> velocity, float rotationTime)
    : GMlib::PSphere<float>(float(size)) {

    _mass = mass;
    _velocity = velocity;

    _force = GMlib::Vector<T, 3>(0, 0, 0);
    _prev_force = GMlib::Vector<T, 3>(0, 0, 0);
    _prev_prev_force = GMlib::Vector<T, 3>(0, 0, 0);
    _prev_prev_prev_force = GMlib::Vector<T, 3>(0, 0, 0);

    _step = GMlib::Vector<T, 3>(0, 0, 0);

    _prev_dt = T(0);
    _prev_prev_dt = T(0);

    _rot_angle_pr_sec = rotationTime != 0 ? M_2PI / rotationTime : 0;

    _on_surface = false;

    _time_to_live = 0;
    _time_alive = 0;
    _is_expired = false;
}


template <typename T>
void SpaceObject<T>::addForce(GMlib::Vector<T, 3> newForce){

    _force += newForce;
}


/**
     * Computes the next step based on velocity and forces
     */
template <typename T>
GMlib::Vector<T, 3> SpaceObject<T>::calculateStep(T dt) {

    // Find the derivatives of the force
    GMlib::Vector<T, 3> fd = (_prev_force - _force) / dt;
    GMlib::Vector<T, 3> fdd = (_prev_prev_force - _force) / (dt + _prev_dt);
    GMlib::Vector<T, 3> fddd = (_prev_prev_prev_force - _force) / (dt + _prev_dt + _prev_prev_dt);

    GMlib::Vector<T, 3> ds = dt * _velocity
            + (((1 / 2) * (dt * dt)) / _mass) * (_force / _mass)
            + (((1 / 6) * (dt * dt * dt)) / _mass) * (fd / _mass)
            + (((1 / 24) * (dt * dt * dt * dt)) / _mass) * (fdd / _mass)
            + (((1 / 120) * (dt * dt * dt * dt * dt)) / _mass) * (fddd / _mass);

    _velocity += dt * (_force / _mass)
            + (1 / 2) * (dt * dt) * ( fd / _mass)
            + (1 / 6) * (dt * dt * dt) * (fdd / _mass)
            + (1 / 24) * (dt * dt * dt * dt) * (fddd / _mass);

    // Store previous forces, and reset force vector
    _prev_prev_prev_force = _prev_prev_force;
    _prev_prev_force = _prev_force;
    _prev_force = _force;
    _force = GMlib::Vector<T, 3>(0, 0, 0);

    _prev_prev_dt = _prev_dt;
    _prev_dt = dt;

    return ds;
}


template <typename T>
GMlib::Vector<T, 3> SpaceObject<T>::getStep() const {

    return _step;
}


template <typename T>
void SpaceObject<T>::setOnSurface(std::shared_ptr<SpaceObject<T> > surface) {

    _surface = surface;
    _on_surface = true;

}


template <typename T>
void SpaceObject<T>::setStep(GMlib::Vector<T, 3> step) {

    _step = step;
}


template <typename T>
void SpaceObject<T>::setVelocity(GMlib::Vector<T, 3> newVelocity) {

    _velocity = newVelocity;
}


template <typename T>
GMlib::Vector<T, 3> SpaceObject<T>::getVelocity() const {

    return _velocity;
}


template <typename T>
T SpaceObject<T>::getMass() const {

    return _mass;
}


template <typename T>
void SpaceObject<T>::setDespawnTime(T lifespan) {

    _time_to_live = lifespan;
}


template <typename T>
bool SpaceObject<T>::isExpired() const {

    return _is_expired;
}


template <typename T>
void SpaceObject<T>::localSimulate(double dt) {

    // Some planets may rotate
    if (_rot_angle_pr_sec != 0) {

        this->rotate(GMlib::Angle(_rot_angle_pr_sec  * _prev_dt), getParent()->getUp());
    }

    // Some objects may have a limited lifespan
    if (_time_to_live != 0) {
        _time_alive += dt;

        if (_time_alive >= _time_to_live)
            _is_expired = true;

    }


    GMlib::Vector<float, 3> ds(_step(0), _step(1), _step(2));

    this->translateGlobal(ds);

}
}
