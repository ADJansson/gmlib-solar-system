uniform mat4 u_mvmat, u_mvpmat, u_pmat;
uniform bool u_billboard;

in vec4 in_vertex;

smooth out vec2 ex_tex;

out vec4 gl_Position;

void main() {

    vec4 v_pos = u_mvmat * in_vertex;

    ex_tex = gl_MultiTexCoord0.xy;

    // Billboarding
    mat4 billMat = u_mvmat;

    if (u_billboard) {

        billMat[0][0] = 1.0;
        billMat[0][1] = 0.0;
        billMat[0][2] = 0.0;

        billMat[1][0] = 0.0;
        billMat[1][1] = 1.0;
        billMat[1][2] = 0.0;

        billMat[2][0] = 0.0;
        billMat[2][1] = 0.0;
        billMat[2][2] = 1.0;
    }

    gl_Position = u_pmat * billMat * in_vertex;
}
