/*! \file collisionobject.c
 *
 *  Implementation of the collisionobject template class.
 */

#include "../include/collisionobject.h"
namespace ADJlib {

template <typename T>
void CollisionObject<T>::make(std::shared_ptr<SpaceObject<T> > o1, std::shared_ptr<SpaceObject<T> > o2, T x, GMlib::Vector<T, 3> dp) {

    _object1 = o1;
    _object2 = o2;
    _x = x;

    calculateNewVelocity(dp);
}


template <typename T>
void CollisionObject<T>::calculateNewVelocity(GMlib::Vector<T, 3> dp) {

    T m1 = _object1->getMass();
    T m2 = _object2->getMass();
    GMlib::Vector<T, 3> u1 = _object1->getVelocity();
    GMlib::Vector<T, 3> u2 = _object2->getVelocity();

    _new_v1 = u1 - (2 * m2) / (m1 + m2) * (((u1 - u2) * (dp)) / ((dp) * (dp))) * (dp);
    _new_v2 = u2 - (2 * m1) / (m1 + m2) * (((u2 - u1) * (-dp)) / ((-dp) * (-dp))) * (-dp);

    _new_ds1 = _x * _object1->getStep();
    _new_ds2 = _x * _object2->getStep();

}


template <typename T>
T CollisionObject<T>::getX() const {

    return _x;
}


template <typename T>
std::shared_ptr<SpaceObject<T> > CollisionObject<T>::getObject1() const {

    return _object1;
}


template <typename T>
std::shared_ptr<SpaceObject<T> > CollisionObject<T>::getObject2() const {

    return _object2;
}


template <typename T>
GMlib::Vector<T, 3> CollisionObject<T>::getNewV1() const {

    return _new_v1;
}


template <typename T>
GMlib::Vector<T, 3> CollisionObject<T>::getNewV2() const {

    return _new_v2;
}


template <typename T>
GMlib::Vector<T, 3> CollisionObject<T>::getNewDs2() const {

    return _new_ds2;
}


template <typename T>
GMlib::Vector<T, 3> CollisionObject<T>::getNewDs1() const {

    return _new_ds1;
}


template <typename T>
const bool CollisionObject<T>::operator <(const CollisionObject<T> &c) {

    return _x < c.getX();
}


template <typename T>
const bool CollisionObject<T>::operator ==(const CollisionObject<T> &c) {

    if (_object1 == c.getObject1())
        return true;
    if (_object1 == c.getObject2())
        return true;
    if (_object2 == c.getObject1())
        return true;
    if (_object2 == c.getObject2())
        return true;

    return false;
}
}
