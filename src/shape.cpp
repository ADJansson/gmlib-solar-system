#include "../include/shape.h"

namespace ADJlib {

Shape::Shape(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices, std::vector<glm::vec3> normals,
             GMlib::GL::Texture tex, bool textured, std::vector<glm::vec2> texCoords, GMlib::Point<float, 3> color) {

    _texCoordsBuffer.create();
    _indexBuffer.create();
    _normalBuffer.create();
    _vertexBuffer.create();

    _vertexArray = vertices;
    _indexArray = indices;
    _normalArray = normals;
    _texCoords = texCoords;
    _texture = tex;
    _color = color;

    _isTextured = textured;

    // Fill buffers
    _vertexBuffer.bufferData(_vertexArray.size() * sizeof(GMlib::GL::GLVertex), &_vertexArray[0], GL_STATIC_DRAW);
    _indexBuffer.bufferData(_indexArray.size() * sizeof(GLuint), &_indexArray[0], GL_STATIC_DRAW);
    _normalBuffer.bufferData(_normalArray.size() * sizeof(GMlib::GL::GLNormal), &_normalArray[0], GL_STATIC_DRAW);
    if (_isTextured)
        _texCoordsBuffer.bufferData(_texCoords.size() * sizeof(GMlib::GL::GLTex2D), &_texCoords[0], GL_STATIC_DRAW);
}

Shape::~Shape() {

    _vertexBuffer.unmapBuffer();
    _indexBuffer.unmapBuffer();
    _normalBuffer.unmapBuffer();
    _texCoordsBuffer.unmapBuffer();
}

GMlib::Point<float, 3> Shape::getColor() const {

    return _color;
}

GMlib::GL::Texture Shape::getTexture() const {

    return _texture;
}

bool Shape::isTextured() const {

    return _isTextured;
}

void Shape::render(GMlib::GL::AttributeLocation vert_loc, GMlib::GL::AttributeLocation norm_loc, GMlib::GL::AttributeLocation tex_loc) const {

    // Bind and draw
    _normalBuffer.bind();
    _normalBuffer.enable(norm_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLNormal), reinterpret_cast<const GLvoid *>(0x0));

    if (_isTextured) {

        _texCoordsBuffer.bind();
        _texCoordsBuffer.enable(tex_loc, 2, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLTex2D), reinterpret_cast<const GLvoid *>(0x0) );
    }

    renderGeo(vert_loc);

    if (_isTextured) {

        _texCoordsBuffer.disable(tex_loc);
        _texCoordsBuffer.unbind();
    }

    _normalBuffer.disable(norm_loc);
    _normalBuffer.unbind();
}

void Shape::renderGeo(GMlib::GL::AttributeLocation vert_loc) const {

    _vertexBuffer.bind();
    _vertexBuffer.enable(vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertex), reinterpret_cast<const GLvoid *>(0x0) );

    _indexBuffer.bind();
    _indexBuffer.drawElements( GL_TRIANGLES, _indexArray.size(), GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(0x0));
    _indexBuffer.unbind();

    _vertexBuffer.disable( vert_loc );
    _vertexBuffer.unbind();
}
}
