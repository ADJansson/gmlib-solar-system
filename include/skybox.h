#ifndef SKYBOX_H
#define SKYBOX_H

#include "skyboxvisualizer.h"

#include <scene/gmsceneobject>
#include <memory>

namespace ADJlib {

/*! \class Skybox
 *
 * Scene object to hold skybox visualizer. Uses SkyboxVisualizer to render
 * the skybox.
 *
 * Andreas Dyrøy Jansson 2016
 */
class Skybox :  public GMlib::SceneObject {
    GM_SCENEOBJECT(Skybox)

public:
    Skybox();
};
}

#endif // SKYBOX_H
