#ifndef MODELLOADER_H
#define MODELLOADER_H

#include "shape.h"
#include "textureloader.h"
#include "glm/glm.hpp"
#include "tinyobjloader/tiny_obj_loader.h"

#include <gmParametricsModule>

// qt
#include <QImage>
#include <QOpenGLTexture>
#include <QImageReader>
#include <QDirIterator>

#include <string>
#include <memory>

namespace ADJlib {

/*! \class ModelLoader
 *
 * Uses library Tiny obj loader (http://syoyo.github.io/tinyobjloader/)
 * to read model data and material files. Uses Qt functions to load image files
 * and make openGL textures for GMlib.
 *
 * Andreas Dyrøy Jansson 2016
 */
class ModelLoader {

public:
    std::vector<std::shared_ptr<Shape>> loadModel(const std::string folder, const std::string fileName);

private:
    const std::string _baseFolderPath = "models\\";
};
}

#endif // MODELLOADER_H
