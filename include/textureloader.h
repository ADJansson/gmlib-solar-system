#ifndef TEXTURELOADER
#define TEXTURELOADER
#include <vector>
#include <gmParametricsModule>

// qt
#include <QImage>
#include <QOpenGLTexture>
#include <QImageReader>
#include <QDirIterator>

namespace GMlib {
template <typename T, int n>
class  PSurfTexVisualizer;
}


namespace ADJlib {
/**
     * Loads texture files from folder
     */
class TextureLoader {

private:
    std::vector<GMlib::PSurfTexVisualizer<float, 3>*> loadTextures(QString folderPath) const;
    const QString _baseFolderPath = "img/";

public:
    std::vector<GMlib::PSurfTexVisualizer<float, 3>*> loadPlanetTextures() const;
    std::vector<GMlib::PSurfTexVisualizer<float, 3>*> loadStarshipTextures() const;
    GMlib::PSurfTexVisualizer<float, 3>* loadVisualizer(const QString fileName) const;
    GMlib::GL::Texture loadGLTexture(const QString fileName) const;
};
}

#endif // TEXTURELOADER

