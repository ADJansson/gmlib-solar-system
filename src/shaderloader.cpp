#include "../include/shaderloader.h"

namespace ADJlib {

GMlib::GL::Program ShaderLoader::loadShader(const std::string prog_name) const {

    GMlib::GL::Program program;

    if( program.acquire(prog_name))
        return program;

    // Read the shader source files
    std::string vs_src = GMlib::GL::OpenGLManager::glslDefHeaderVersionSource()
            + readShaderSource(prog_name, EVertexShader);

    std::string fs_src = GMlib::GL::OpenGLManager::glslDefHeaderVersionSource()
            + readShaderSource(prog_name, EFragmentShader);

    // Copied from GMlib::PSurfTexVisualizer::initShaderProgram
    bool compile_ok, link_ok;

    GMlib::GL::VertexShader vshader;
    vshader.create(prog_name + "_vs");
    vshader.setPersistent(true);
    vshader.setSource(vs_src);
    compile_ok = vshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << vshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << vshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    GMlib::GL::FragmentShader fshader;
    fshader.create(prog_name + "_fs");
    fshader.setPersistent(true);
    fshader.setSource(fs_src);
    compile_ok = fshader.compile();
    if( !compile_ok ) {
        std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
        std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    program.create(prog_name);
    program.setPersistent(true);
    program.attachShader(vshader);
    program.attachShader(fshader);
    link_ok = program.link();
    if( !link_ok ) {
        std::cout << "Error: " << program.getLinkerLog() << std::endl;
    }
    assert(link_ok);

    return program;
}

std::string ShaderLoader::readShaderSource(std::string fileName, EShaderType shaderType) const {

    switch (shaderType) {
    case EVertexShader:
        fileName += ".vert";
        break;
    case EFragmentShader:
        fileName += ".frag";
        break;
    default:
        std::cerr << "ERROR: unknown shader file type" << std::endl;
        exit(1);
        break;
    }

    // Open the file
    std::ifstream shaderfile(_shaderFolderPath + fileName);
    std::string contents;

    if (shaderfile.is_open())
        contents = std::string((std::istreambuf_iterator<char>(shaderfile)), std::istreambuf_iterator<char>());
    else
        std::cerr << "ERROR: could not open file " << fileName.c_str() << std::endl;

    return contents;
}
}
