#include "../include/model.h"

namespace ADJlib {

Model::Model(const std::string folder, const std::string fileName) {

    this->setSurroundingSphere(GMlib::Sphere<float, 3>(100.0f));

    ModelLoader mLoader;
    auto modelViz = new ModelVisualizer();
    modelViz->setModelData(mLoader.loadModel(folder, fileName)); // Send model data to visualizer
    insertVisualizer(modelViz);
}
}
