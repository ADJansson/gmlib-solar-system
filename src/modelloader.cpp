#define TINYOBJLOADER_IMPLEMENTATION
#include "../include/modelloader.h"

namespace ADJlib {

/**
 * I have been working on model loading before, as a hobby project
 * (https://source.uit.no/aja073/TheModelLoaderRC).
 * This implementation is modified to work with GMlib and Qt.
 */
std::vector<std::shared_ptr<Shape>> ModelLoader::loadModel(const std::string folder, const std::string fileName) {

    std::vector<std::shared_ptr<Shape>> shapes; // Shapes to return. obj-files are made up of one or more shapes
    std::vector<tinyobj::shape_t> tolShapes; // Loaded shapes
    std::vector<tinyobj::material_t> tolMaterials; // Loaded materials

    const std::string file = _baseFolderPath + folder + fileName;

    // Read the file
    std::string err;
    bool ret = tinyobj::LoadObj(tolShapes, tolMaterials, err, file.c_str(), (_baseFolderPath + folder).c_str());

    if (!err.empty())
        std::cerr << err << std::endl;

    if (!ret)
        exit(1);

    std::cout << "# of shapes    : " << tolShapes.size() << std::endl;
    std::cout << "# of materials : " << tolMaterials.size() << std::endl;

    // Keep track of loaded textures, they may be reused
    std::map <std::string, GMlib::GL::Texture> texPaths;

    for (int i = 0; i < tolShapes.size(); i++) {

        std::vector<unsigned int> indexArray;
        std::vector<glm::vec3> vertexArray;
        std::vector<glm::vec3> normalArray;
        std::vector<glm::vec2> texCoords;

        auto currentShape = tolShapes[i];

        indexArray.clear();
        vertexArray.clear();
        normalArray.clear();
        texCoords.clear();

        // Check dimension of indices (should be 3D)
        assert((currentShape.mesh.indices.size() % 3) == 0);

        // Index array
        for (int f = 0; f < currentShape.mesh.indices.size() / 3; f++) {

            indexArray.push_back(currentShape.mesh.indices[3 * f + 0]);
            indexArray.push_back(currentShape.mesh.indices[3 * f + 1]);
            indexArray.push_back(currentShape.mesh.indices[3 * f + 2]);
        }

        // Check vertex dimension (should be 3D)
        assert((currentShape.mesh.positions.size() % 3) == 0);

        // Vertex array
        for (int v = 0; v < currentShape.mesh.positions.size() / 3; v++) {

            vertexArray.push_back(glm::vec3( // Flip the coordinate system
                                      currentShape.mesh.positions[3 * v + 2],   // z
                                      currentShape.mesh.positions[3 * v + 0],   // x
                                      currentShape.mesh.positions[3 * v + 1])); // y
        }


        // Check dimension of normals (should be 3D)
        assert((currentShape.mesh.normals.size() % 3) == 0);

        // Normals
        for (int v = 0; v < currentShape.mesh.normals.size() / 3; v++) {

            normalArray.push_back(glm::vec3(
                                      currentShape.mesh.normals[3 * v + 2],   // z
                                      currentShape.mesh.normals[3 * v + 0],   // x
                                      currentShape.mesh.normals[3 * v + 1])); // y
        }

        // Calculate normals manually http://forum.devmaster.net/t/calculating-normals-of-a-mesh/6065/5 25/5-16
        // In case a loaded model doesn't include normals
        if (normalArray.empty()) {

            normalArray = std::vector<glm::vec3>(vertexArray.size(), glm::vec3(0.f, 0.f, 0.f));

            for (int i = 0; i < indexArray.size(); i += 3) {

                auto p1 = vertexArray[indexArray[i + 2]];
                auto p2 = vertexArray[indexArray[i + 0]];
                auto p3 = vertexArray[indexArray[i + 1]];

                glm::vec3 v1 = p2 - p1;
                glm::vec3 v2 = p3 - p1;

                glm::vec3 normal = glm::cross(v1, v2);

                normalArray[indexArray[i + 2]] += normal;
                normalArray[indexArray[i + 0]] += normal;
                normalArray[indexArray[i + 1]] += normal;
            }

            for (auto n : normalArray)
                n = glm::normalize(n);
        }

        // Check dimension of texture coordinates (should be 2D)
        assert((currentShape.mesh.texcoords.size() % 2) == 0);

        // Texture coordinates
        for (int t = 0; t < currentShape.mesh.texcoords.size() / 2; t++) {

            texCoords.push_back(glm::vec2(
                                    currentShape.mesh.texcoords[2 * t + 0],
                                    currentShape.mesh.texcoords[2 * t + 1]));
        }

        // Load materials
        TextureLoader tloader;
        GMlib::GL::Texture texture;
        GMlib::Point<float, 3> color;
        bool textured = false;

        // Check if the current shape has any materials or textures
        if (currentShape.mesh.material_ids.size() > 0) {

            int currentMatID = currentShape.mesh.material_ids[0];

            if (tolMaterials.size() > 0) {

                if (currentMatID < tolMaterials.size()) {

                    auto c = tolMaterials[currentMatID].diffuse;
                    color = GMlib::Point<float, 3>(c[0], c[1], c[2]);

                    // Only diffuse maps are supported for now
                    if (!(tolMaterials[currentMatID].diffuse_texname).empty()) {

                        // A texture was found
                        textured = true;
                        std::string textureFile = _baseFolderPath + folder + tolMaterials[currentMatID].diffuse_texname;

                        // Check if texture has already been loaded
                        if (texPaths.find(textureFile) == texPaths.end()) {

                            // Use Qt to load the texture file

                            std::cout << "Loading texture " << textureFile << std::endl;

                            const QString qTextureFile = QString::fromStdString(textureFile);

                            texture = tloader.loadGLTexture(qTextureFile);

                            texPaths.insert(std::pair<std::string, GMlib::GL::Texture>(textureFile, texture));
                        }
                        else {
                            texture = texPaths[textureFile]; // Reuse loaded textures
                        }
                    }
                }
            }
        }

        auto shape = std::make_shared<Shape>(vertexArray, indexArray, normalArray, texture, textured, texCoords, color);
        shapes.push_back(shape);
    }

    return shapes;
}
}
