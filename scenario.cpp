#include "scenario.h"

#include "include/skybox.h"
#include "include/controller.h"

//// hidmanager
//#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <gmOpenglModule>
#include <gmSceneModule>
#include <gmParametricsModule>

// qt
#include <QQuickItem>



void Scenario::initializeScenario() {

    // Insert a light
    GMlib::Point<GLfloat,3> init_light_pos( 0, 0, 0 );
    GMlib::PointLight *light = new GMlib::PointLight(  GMlib::GMcolor::white(), GMlib::GMcolor::white(),
                                                       GMlib::GMcolor::white(), init_light_pos );
    light->setAttenuation(0.8, 0.002, 0.0008);
    scene()->insertLight( light, false );

    // Default camera parameters
    int init_viewport_size = 600;
    GMlib::Point<float,3> init_cam_pos(  0.0f, 0.0f, 0.0f );
    GMlib::Vector<float,3> init_cam_dir( 1.0f, 0.0f, 0.0f );
    GMlib::Vector<float,3> init_cam_up( 0.0f, 0.0f, 1.0f );

    // Projection cam
    auto proj_rcpair = createRCPair("Projection");
    proj_rcpair.camera->set(init_cam_pos, init_cam_dir, init_cam_up);
    proj_rcpair.camera->setCuttingPlanes( 1.f, 1e10 );
    proj_rcpair.camera->rotateGlobal( GMlib::Angle(20), GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f ) );
    proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( -25.0f, 0.0f, 10.0f ) );
    scene()->insertCamera( proj_rcpair.camera.get() );
    proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

    // Front cam
    auto front_rcpair = createRCPair("Front");
    front_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, -5.0f, 0.0f ), init_cam_dir, init_cam_up );
    front_rcpair.camera->setCuttingPlanes( 0.5f, 1e10 );
    scene()->insertCamera( front_rcpair.camera.get() );
    front_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

    // Side cam
    auto side_rcpair = createRCPair("Side");
    side_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( -5.0f, 0.0f, 0.0f ), GMlib::Vector<float,3>( 1.0f, 0.0f, 0.0f ), init_cam_up );
    side_rcpair.camera->setCuttingPlanes( 0.5f, 1e10 );
    scene()->insertCamera( side_rcpair.camera.get() );
    side_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

    // Top cam
    auto top_rcpair = createRCPair("Top");
    top_rcpair.camera->set( init_cam_pos + GMlib::Vector<float,3>( 0.0f, 0.0f, 5.0f ), -init_cam_up, init_cam_dir );
    top_rcpair.camera->setCuttingPlanes( 0.5f, 1e10 );
    scene()->insertCamera( top_rcpair.camera.get() );
    top_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );


    // Add skybox first
    _skybox = std::make_shared<ADJlib::Skybox>();
    scene()->insert(_skybox.get());

    _controller = new ADJlib::Controller<double>(this, ADJlib::Mode::Game);
    scene()->insert(_controller);
    _controller->makeSolarSystem(proj_rcpair.camera, light);

}

void Scenario::cleanupScenario() {

}

void Scenario::controlForward() {

    _controller->addThrustStarship(1);
}

void Scenario::controlReverse() {

    _controller->addThrustStarship(-1);
}

void Scenario::controlPitchDown() {

    _controller->pitchStarship(2);
}

void Scenario::controlPitchUp() {

    _controller->pitchStarship(-2);
}

void Scenario::controlRollLeft() {

    _controller->rollStarship(-2);
}

void Scenario::controlRollRight() {

    _controller->rollStarship(2);
}

void Scenario::controlYawLeft() {

    _controller->yawStarship(2);
}

void Scenario::controlYawRight() {

    _controller->yawStarship(-2);
}

void Scenario::controlReleasePitch() {

    _controller->pitchStarship(0);
}

void Scenario::controlReleaseRoll() {

    _controller->rollStarship(0);
}

void Scenario::controlReleaseYaw() {

    _controller->yawStarship(0);
}

void Scenario::controlShootPhoton() {

    _controller->fireTorpedo();
}

void Scenario::controlEngageWarp(int factor) {

    //std::cout << "pressed value: " << id << std::endl;
    _controller->engageWarp(factor);
}

void Scenario::controlIncreaseSimSpeed() {

    std::cout << "pressed value: " << std::endl;
    _controller->increaseSimSpeed();
}

void Scenario::controlDecreaseSimSpeed() {

    _controller->decreaseSimSpeed();
}
