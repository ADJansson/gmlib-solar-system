
#include "../include/starship.h"

namespace ADJlib {

Starship::Starship() {

    TextureLoader loader;

    auto textures = loader.loadStarshipTextures();

    this->setSurroundingSphere(GMlib::Sphere<float,3>(100.0f));

    // BEGIN Saucer section set-up
    auto saucer = std::make_shared<GMlib::PCylinder<float>>(6.4, 6.4, 0.7);
    saucer->insertVisualizer(textures[2]);
    saucer->replot(200,200,1,1);
    saucer->setMaterial(GMlib::GMmaterial::silver());
    insert(saucer.get());
    _parts.push_back(saucer);

    auto saucerTop = std::make_shared<GMlib::PCone<float>>(3.2, 0.4);
    saucerTop->insertVisualizer(textures[1]);
    saucerTop->translate( GMlib::Vector<float,3>(0, 0, 0.75));
    saucerTop->replot(200,200,1,1);
    saucerTop->setMaterial(GMlib::GMmaterial::silver());
    insert(saucerTop.get());
    _parts.push_back(saucerTop);

    auto bridgeDome = std::make_shared<GMlib::PSphere<float>>(0.5);
    bridgeDome->toggleDefaultVisualizer();
    bridgeDome->translate( GMlib::Vector<float,3>(0, 0, 1));
    bridgeDome->replot(200,200,1,1);
    bridgeDome->setMaterial(GMlib::GMmaterial::snow());
    insert(bridgeDome.get());
    _parts.push_back(bridgeDome);

    auto sensorDome = std::make_shared<GMlib::PSphere<float>>(0.5);
    sensorDome->toggleDefaultVisualizer();
    sensorDome->translate( GMlib::Vector<float,3>(0, 0, -0.8));
    sensorDome->replot(200,200,1,1);
    sensorDome->setMaterial(GMlib::GMmaterial::snow());
    insert(sensorDome.get());
    _parts.push_back(sensorDome);

    auto saucerBtm = std::make_shared<GMlib::PCone<float>>(3.2, -0.4);
    saucerBtm->toggleDefaultVisualizer();
    saucerBtm->translate( GMlib::Vector<float,3>(0, 0, -0.75));
    saucerBtm->replot(200,200,1,1);
    saucerBtm->setMaterial(GMlib::GMmaterial::silver());
    insert(saucerBtm.get());
    _parts.push_back(saucerBtm);

    auto impulseEngine = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                                GMlib::Vector<float, 3>(0, 2, 0),
                                                                GMlib::Vector<float, 3>(0, 0, 0.7));
    impulseEngine->toggleDefaultVisualizer();
    impulseEngine->translate( GMlib::Vector<float,3>(-6.4, -1, -0.4));
    impulseEngine->replot(200,200,1,1);
    impulseEngine->setMaterial(GMlib::GMmaterial::silver());
    insert(impulseEngine.get());
    _parts.push_back(impulseEngine);

    auto impulseVentRgt = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                                 GMlib::Vector<float, 3>(0, 0.5, 0),
                                                                 GMlib::Vector<float, 3>(0, 0, 0.4));
    impulseVentRgt->toggleDefaultVisualizer();
    impulseVentRgt->translate( GMlib::Vector<float,3>(-6.5, -0.7, -0.2));
    impulseVentRgt->replot(200,200,1,1);
    impulseVentRgt->setMaterial(GMlib::GMmaterial::blackPlastic());
    insert(impulseVentRgt.get());
    _parts.push_back(impulseVentRgt);

    auto impulseVentLft = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                                 GMlib::Vector<float, 3>(0, -0.5, 0),
                                                                 GMlib::Vector<float, 3>(0, 0, 0.4));
    impulseVentLft->toggleDefaultVisualizer();
    impulseVentLft->translate( GMlib::Vector<float,3>(-6.5, 0.7, -0.2));
    impulseVentLft->replot(200,200,1,1);
    impulseVentLft->setMaterial(GMlib::GMmaterial::blackPlastic());
    insert(impulseVentLft.get());
    _parts.push_back(impulseVentLft);

    // END Saucer section set-up

    // BEGIN Hull set-up

    auto hull = std::make_shared<GMlib::PCylinder<float>>(1.3, 1.3, 9.5);
    hull->toggleDefaultVisualizer();
    hull->rotate(GMlib::Angle(90), GMlib::Vector<float,3>(0, 1, 0));
    hull->translate(GMlib::Vector<float,3>(4, 0, -9.8));
    hull->replot(200,200,1,1);
    hull->setMaterial(GMlib::GMmaterial::silver());
    insert(hull.get());
    _parts.push_back(hull);

    auto hullFront = std::make_shared<GMlib::PCone<float>>(0.65, 0.1);
    hullFront->toggleDefaultVisualizer();
    hullFront->rotate(GMlib::Angle(90), GMlib::Vector<float,3>(0, 1, 0));
    hullFront->translate(GMlib::Vector<float,3>(4, 0, -4.96));
    hullFront->replot(200,200,1,1);
    hullFront->setMaterial(GMlib::GMmaterial::silver());
    insert(hullFront.get());
    _parts.push_back(hullFront);

    auto hullBack = std::make_shared<GMlib::PCone<float>>(0.65, -0.4);
    hullBack->toggleDefaultVisualizer();
    hullBack->rotate( GMlib::Angle(90), GMlib::Vector<float,3>(0, 1, 0));
    hullBack->translate( GMlib::Vector<float,3>(4, 0, -14.95));
    hullBack->replot(200,200,1,1);
    hullBack->setMaterial(GMlib::GMmaterial::silver());
    insert(hullBack.get());
    _parts.push_back(hullBack);

    auto dish = std::make_shared<GMlib::PCone<float>>(0.45, -0.2);
    dish->toggleDefaultVisualizer();
    dish->rotate( GMlib::Angle(90), GMlib::Vector<float,3>( 0, 1, 0));
    dish->translate( GMlib::Vector<float,3>( 4, 0, -4.66) );
    dish->replot(200,200,1,1);
    dish->setMaterial(GMlib::GMmaterial::gold());
    insert(dish.get());
    _parts.push_back(dish);

    auto antennae = std::make_shared<GMlib::PCone<float>>(0.14, 1);
    antennae->toggleDefaultVisualizer();
    antennae->rotate( GMlib::Angle(90), GMlib::Vector<float,3>( 0, 1, 0));
    antennae->translate( GMlib::Vector<float,3>( 4, 0, -4.66) );
    antennae->replot(200,200,1,1);
    antennae->setMaterial(GMlib::GMmaterial::gold());
    insert(antennae.get());
    _parts.push_back(antennae);

    // END Hull set-up

    // BEGIN Neck set-up

    auto neckR = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                        GMlib::Vector<float, 3>(3, 0, 0),
                                                        GMlib::Vector<float, 3>(3.3, 0, 3));
    neckR->toggleDefaultVisualizer();
    neckR->translate( GMlib::Vector<float,3>( -9, -0.3, -3));
    neckR->replot(200,200,1,1);
    neckR->setMaterial(GMlib::GMmaterial::silver());
    insert(neckR.get());
    _parts.push_back(neckR);

    auto neckL = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                        GMlib::Vector<float, 3>(3, 0, 0),
                                                        GMlib::Vector<float, 3>(3.3, 0, 3));
    neckL->toggleDefaultVisualizer();
    neckL->translate( GMlib::Vector<float,3>(-9, 0.3, -3));
    neckL->replot(200,200,1,1);
    neckL->setMaterial(GMlib::GMmaterial::silver());
    insert(neckL.get());
    _parts.push_back(neckL);

    auto neckB = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                        GMlib::Vector<float, 3>(0, 0.6, 0),
                                                        GMlib::Vector<float, 3>(3.3, 0, 3));
    neckB->toggleDefaultVisualizer();
    neckB->translate( GMlib::Vector<float,3>(-9, -0.3, -3));
    neckB->replot(200,200,1,1);
    neckB->setMaterial(GMlib::GMmaterial::silver());
    insert(neckB.get());
    _parts.push_back(neckB);

    auto neckF = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                        GMlib::Vector<float, 3>(0, 0.6, 0),
                                                        GMlib::Vector<float, 3>(3.3, 0, 3));
    neckF->toggleDefaultVisualizer();
    neckF->translate( GMlib::Vector<float,3>(-6, -0.3, -3));
    neckF->replot(200,200,1,1);
    neckF->setMaterial(GMlib::GMmaterial::silver());
    insert(neckF.get());
    _parts.push_back(neckF);

    // END Neck set-up

    // BEGIN Engines set-up
    // Left side

    auto engineLft = std::make_shared<GMlib::PCylinder<float>>(0.8, 0.8, 14.7);
    engineLft->toggleDefaultVisualizer();
    engineLft->rotate( GMlib::Angle(90), GMlib::Vector<float,3>(0, 1, 0));
    engineLft->translate( GMlib::Vector<float,3>(-1.4, -4.6, -16));
    engineLft->replot(200,200,1,1);
    engineLft->setMaterial(GMlib::GMmaterial::silver());
    insert(engineLft.get());
    _parts.push_back(engineLft);

    auto pylonLft1 = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                            GMlib::Vector<float, 3>(2, 0, 0),
                                                            GMlib::Vector<float, 3>(0, -3.6, 4.5));
    pylonLft1->toggleDefaultVisualizer();
    pylonLft1->translate( GMlib::Vector<float,3>(-12, -1.2, -3.5));
    pylonLft1->replot(200,200,1,1);
    pylonLft1->setMaterial(GMlib::GMmaterial::silver());
    insert(pylonLft1.get());
    _parts.push_back(pylonLft1);

    auto pylonLft2 = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                            GMlib::Vector<float, 3>(2, 0, 0),
                                                            GMlib::Vector<float, 3>(0, -3.6, 4.5));
    pylonLft2->toggleDefaultVisualizer();
    pylonLft2->translate( GMlib::Vector<float,3>(-12, -1, -3.5));
    pylonLft2->replot(200,200,1,1);
    pylonLft2->setMaterial(GMlib::GMmaterial::silver());
    insert(pylonLft2.get());
    _parts.push_back(pylonLft2);

    auto pylonLft3 = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                            GMlib::Vector<float, 3>(0, 0.2, 0),
                                                            GMlib::Vector<float, 3>(0, -3.6, 4.5));
    pylonLft3->toggleDefaultVisualizer();
    pylonLft3->translate( GMlib::Vector<float,3>(-11, -1.2, -3.5));
    pylonLft3->replot(200,200,1,1);
    pylonLft3->setMaterial(GMlib::GMmaterial::silver());
    insert(pylonLft3.get());
    _parts.push_back(pylonLft3);

    auto buzzardLft = std::make_shared<GMlib::PSphere<float>>(0.8);
    buzzardLft->insertVisualizer(textures[0]);
    buzzardLft->rotate( GMlib::Angle(90), GMlib::Vector<float,3>(0, 1, 0));
    buzzardLft->translate( GMlib::Vector<float,3>(-1.4, -4.6, -8.6));
    buzzardLft->replot(200,200,1,1);
    buzzardLft->setMaterial(GMlib::GMmaterial::silver());
    insert(buzzardLft.get());
    _parts.push_back(buzzardLft);

    auto jetLft = std::make_shared<GMlib::PSphere<float>>(0.8);
    jetLft->toggleDefaultVisualizer();
    jetLft->rotate( GMlib::Angle(90), GMlib::Vector<float,3>(0, 1, 0));
    jetLft->translate( GMlib::Vector<float,3>(-1.4, -4.6, -23.3));
    jetLft->replot(200,200,1,1);
    jetLft->setMaterial(GMlib::GMmaterial::snow());
    insert(jetLft.get());
    _parts.push_back(jetLft);

    // Right side

    auto engineRgt = std::make_shared<GMlib::PCylinder<float>>(0.8, 0.8, 14.7);
    engineRgt->toggleDefaultVisualizer();
    engineRgt->rotate( GMlib::Angle(90), GMlib::Vector<float,3>(0, 1, 0));
    engineRgt->translate( GMlib::Vector<float,3>(-1.4, 4.6, -16));
    engineRgt->replot(200,200,1,1);
    engineRgt->setMaterial(GMlib::GMmaterial::silver());
    insert(engineRgt.get());
    _parts.push_back(engineRgt);

    auto pylonRgt1 = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                            GMlib::Vector<float, 3>(2, 0, 0),
                                                            GMlib::Vector<float, 3>(0, 3.6, 4.5));
    pylonRgt1->toggleDefaultVisualizer();
    pylonRgt1->translate( GMlib::Vector<float,3>(-12, 1.2, -3.5));
    pylonRgt1->replot(200,200,1,1);
    pylonRgt1->setMaterial(GMlib::GMmaterial::silver());
    insert(pylonRgt1.get());
    _parts.push_back(pylonRgt1);

    auto pylonRgt2 = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                            GMlib::Vector<float, 3>(2, 0, 0),
                                                            GMlib::Vector<float, 3>(0, 3.6, 4.5));
    pylonRgt2->toggleDefaultVisualizer();
    pylonRgt2->translate( GMlib::Vector<float,3>(-12, 1, -3.5));
    pylonRgt2->replot(200,200,1,1);
    pylonRgt2->setMaterial(GMlib::GMmaterial::silver());
    insert(pylonRgt2.get());
    _parts.push_back(pylonRgt2);

    auto pylonRgt3 = std::make_shared<GMlib::PPlane<float>>(GMlib::Point<float, 3>(0, 0, 0),
                                                            GMlib::Vector<float, 3>(0, -0.2, 0),
                                                            GMlib::Vector<float, 3>(0, 3.6, 4.5));
    pylonRgt3->toggleDefaultVisualizer();
    pylonRgt3->translate( GMlib::Vector<float,3>(-11, 1.2, -3.5));
    pylonRgt3->replot(200,200,1,1);
    pylonRgt3->setMaterial(GMlib::GMmaterial::silver());
    insert(pylonRgt3.get());
    _parts.push_back(pylonRgt3);

    auto buzzardRgt = std::make_shared<GMlib::PSphere<float>>(0.8);
    buzzardRgt->insertVisualizer(textures[0]);
    buzzardRgt->rotate( GMlib::Angle(90), GMlib::Vector<float,3>(0, 1, 0));
    buzzardRgt->translate( GMlib::Vector<float,3>(-1.4, 4.6, -8.6));
    buzzardRgt->replot(200,200,1,1);
    buzzardRgt->setMaterial(GMlib::GMmaterial::silver());
    insert(buzzardRgt.get());
    _parts.push_back(buzzardRgt);

    auto jetRgt = std::make_shared<GMlib::PSphere<float>>(0.8);
    jetRgt->toggleDefaultVisualizer();
    jetRgt->rotate( GMlib::Angle(90), GMlib::Vector<float,3>(0, 1, 0));
    jetRgt->translate( GMlib::Vector<float,3>(-1.4, 4.6, -23.3));
    jetRgt->replot(200,200,1,1);
    jetRgt->setMaterial(GMlib::GMmaterial::snow());
    insert(jetRgt.get());
    _parts.push_back(jetRgt);

    // END Engines set-up

}


Starship::~Starship() {

    _parts.clear();
}
}
