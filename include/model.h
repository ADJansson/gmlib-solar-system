#ifndef MODEL_H
#define MODEL_H

#include "modelloader.h"
#include "modelvisualizer.h"

#include <scene/gmsceneobject>

namespace ADJlib {

/*! \class Model
 *
 * Scene object to hold model information. Uses ModelLoader to read
 * the files and ModelVisualizer to render the model.
 *
 * Andreas Dyrøy Jansson 2016
 */
class Model : public GMlib::SceneObject {
    GM_SCENEOBJECT(Model)

public:
    Model(const std::string folder, const std::string fileName);
};
}


#endif // MODEL_H
