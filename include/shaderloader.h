#ifndef SHADERLOADER_H
#define SHADERLOADER_H

#include <gmParametricsModule>

namespace ADJlib {

// Enum for the two different shader types
enum EShaderType {
    EVertexShader,
    EFragmentShader,
};

/*! \class ShaderLoader
 *
 * Copied from project in STE6249. Loads and compiles shader source code from
 * external shader files.
 *
 * Andreas Dyrøy Jansson 2016
 */
class ShaderLoader {

public:
    GMlib::GL::Program loadShader(const std::string prog_name) const;

private:
    std::string readShaderSource(std::string fileName, EShaderType shaderType) const;
    const std::string _shaderFolderPath = "shaders\\"; // Base folder path
};
}

#endif // SHADERLOADER_H
