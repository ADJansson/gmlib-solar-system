uniform samplerCube skybox_tex;

in vec3 ex_tex;

out vec4 gl_FragColor;

void main() {

    gl_FragColor = texture(skybox_tex, ex_tex);
}
