#ifndef SPACEOBJECT
#define SPACEOBJECT
#include <parametrics/gmpsphere>

namespace ADJlib {
template <typename T>
class SpaceObject : public GMlib::PSphere<float> {
    GM_SCENEOBJECT(SpaceObject)

    private:

        T _mass;
    T _prev_dt;
    T _prev_prev_dt;

    T _time_to_live;
    T _time_alive;
    bool _is_expired;

    GMlib::Vector<T, 3> _force;
    GMlib::Vector<T, 3> _prev_force;
    GMlib::Vector<T, 3> _prev_prev_force;
    GMlib::Vector<T, 3> _prev_prev_prev_force;

    GMlib::Vector<T, 3> _velocity;
    GMlib::Vector<T, 3> _step;
    float _rot_angle_pr_sec;

    bool _on_surface;
    std::shared_ptr<SpaceObject<T>> _surface;

    std::string _name;
    int _id;


public :

    SpaceObject(T size, T mass, GMlib::Vector<T, 3> velocity, float rotationTime = 0);

    void addForce(GMlib::Vector<T, 3> newForce);
    GMlib::Vector<T, 3> calculateStep(T dt);

    void setVelocity(GMlib::Vector<T, 3> newVelocity);
    GMlib::Vector<T, 3> getVelocity() const;

    void setStep(GMlib::Vector<T, 3> step);
    GMlib::Vector<T, 3> getStep() const;

    void setOnSurface(std::shared_ptr<SpaceObject<T>> surface);

    T getMass() const;

    // Used for objects added while running the program
    void setDespawnTime(T lifespan);
    bool isExpired() const;

protected:

    void localSimulate(double dt) override;

};
}

#include "../src/spaceobject.c"
#endif // SPACEOBJECT

