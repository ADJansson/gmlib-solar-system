#include "../include/textureloader.h"

namespace ADJlib {

std::vector<GMlib::PSurfTexVisualizer<float, 3> *> TextureLoader::loadStarshipTextures() const {

    return loadTextures("starship");
}


std::vector<GMlib::PSurfTexVisualizer<float, 3> *> TextureLoader::loadPlanetTextures() const {

    return loadTextures("planets");
}


std::vector<GMlib::PSurfTexVisualizer<float, 3> *> TextureLoader::loadTextures(QString folderPath) const {

    std::vector<GMlib::PSurfTexVisualizer<float, 3>*> visualizers;

    // http://doc.qt.io/qt-4.8/qdir.html
    QString fullPath = _baseFolderPath + folderPath;

    QDir dir(fullPath);
    dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
    dir.setNameFilters(QStringList("tex*.jpg"));

    for (int i = 0; i < dir.count(); i++) {

        GMlib::PSurfTexVisualizer<float, 3> *visualizer = new GMlib::PSurfTexVisualizer<float, 3>;

        QString imagePath = fullPath + "/tex" + QString::number(i) + ".jpg";

        QImage img = QImage(imagePath);

        if(!QFile::exists(imagePath))
            std::cerr << "File " << imagePath.toStdString() << " does not exists" << std::endl;

        if(img.isNull()) {
            std::cerr << "Image file is not supported!" << std::endl;
        }
        assert(!img.isNull());

        img = img.convertToFormat(QImage::Format_RGB888);

        GMlib::GL::Texture texture;

        texture.create(GL_TEXTURE_2D);
        texture.texImage2D(0, GL_RGB, img.width(), img.height(), 0, GL_RGB, GL_UNSIGNED_BYTE, img.bits());
        texture.texParameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        texture.texParameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        texture.texParameterf(GL_TEXTURE_WRAP_S, GL_REPEAT);
        texture.texParameterf(GL_TEXTURE_WRAP_T, GL_REPEAT);

        visualizer->setTexture(texture);
        visualizers.push_back(visualizer);
    }

    return visualizers;
}

GMlib::PSurfTexVisualizer<float, 3>* TextureLoader::loadVisualizer(const QString fileName) const {

    GMlib::PSurfTexVisualizer<float, 3>* visualizer = new GMlib::PSurfTexVisualizer<float, 3>;

    visualizer->setTexture(loadGLTexture(fileName));

    return visualizer;
}

GMlib::GL::Texture TextureLoader::loadGLTexture(const QString fileName) const {

    QImage img = QImage(fileName);

    if(!QFile::exists(fileName))
        std::cerr << "File " << fileName.toStdString() << " does not exist" << std::endl;

    if(img.isNull()) {
        std::cerr << "Image file is not supported!" << std::endl;
    }
    assert(!img.isNull());

    img = img.convertToFormat(QImage::Format_RGB888);

    GMlib::GL::Texture texture;
    texture.create(GL_TEXTURE_2D);
    texture.texImage2D(0, GL_RGB, img.width(), img.height(), 0, GL_RGB, GL_UNSIGNED_BYTE, img.bits());
    texture.texParameteri(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    texture.texParameteri(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    texture.texParameterf(GL_TEXTURE_WRAP_S, GL_REPEAT);
    texture.texParameterf(GL_TEXTURE_WRAP_T, GL_REPEAT);

    return texture;
}
}
