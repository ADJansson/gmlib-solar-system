#ifndef SHAPE_H
#define SHAPE_H

#include "glm/glm.hpp"

#include <gmParametricsModule>

namespace ADJlib {

/*! \class Shape
 *
 * Helper class to hold vertex, index, texture coordinates and normals
 * for each part of the whole model.
 *
 * Andreas Dyrøy Jansson 2016
 */
class Shape {

public:
    Shape(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices, std::vector<glm::vec3> normals,
          GMlib::GL::Texture tex, bool textured = false, std::vector<glm::vec2> texCoords = std::vector<glm::vec2>(),
          GMlib::Point<float, 3> color = GMlib::Point<float, 3>(1.f, 1.f, 1.f));
    ~Shape();

    // Called from visualizer
    void render(GMlib::GL::AttributeLocation vert_loc, GMlib::GL::AttributeLocation norm_loc, GMlib::GL::AttributeLocation tex_loc) const;
    void renderGeo(GMlib::GL::AttributeLocation vert_loc) const;

    GMlib::Point<float, 3> getColor() const;
    GMlib::GL::Texture getTexture() const;
    bool isTextured() const;

private:
    std::vector<glm::vec3> _vertexArray;
    std::vector<unsigned int> _indexArray;
    std::vector<glm::vec3> _normalArray;
    std::vector<glm::vec2> _texCoords;

    bool _isTextured;
    GMlib::Point<float, 3> _color; // Color for phong shading

    GMlib::GL::VertexBufferObject _vertexBuffer;
    GMlib::GL::IndexBufferObject _indexBuffer;
    GMlib::GL::VertexBufferObject _normalBuffer;
    GMlib::GL::VertexBufferObject _texCoordsBuffer;
    GMlib::GL::Texture _texture; // Diffuse texture
};
}

#endif // SHAPE_H
