uniform mat3 u_rmat;

in vec4 in_vertex;

out vec3 ex_tex;
out vec4 gl_Position;

void main() {

    // u_mvpmat = projection * view
    gl_Position = in_vertex;
    ex_tex = in_vertex.xyz * u_rmat;
}
