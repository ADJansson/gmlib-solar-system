#ifndef COLLISIONOBJECT_H
#define COLLISIONOBJECT_H
#include "spaceobject.h"

namespace ADJlib {

template <typename T>
class CollisionObject {

private:

    T _x;
    std::shared_ptr<SpaceObject<T>> _object1;
    std::shared_ptr<SpaceObject<T>> _object2;
    GMlib::Vector<T, 3> _new_v1;
    GMlib::Vector<T, 3> _new_v2;
    GMlib::Vector<T, 3> _new_ds1;
    GMlib::Vector<T, 3> _new_ds2;

public:

    void make(std::shared_ptr<SpaceObject<T>> o1, std::shared_ptr<SpaceObject<T>> o2, T x, GMlib::Vector<T, 3> dp);
    void calculateNewVelocity(GMlib::Vector<T, 3> dp);

    std::shared_ptr<SpaceObject<T>> getObject1() const;
    std::shared_ptr<SpaceObject<T>> getObject2() const;

    T getX() const;

    GMlib::Vector<T, 3> getNewV1() const;
    GMlib::Vector<T, 3> getNewV2() const;
    GMlib::Vector<T, 3> getNewDs1() const;
    GMlib::Vector<T, 3> getNewDs2() const;

    const bool operator < (const CollisionObject<T> &c);
    const bool operator == (const CollisionObject<T> &c);
};
}
#include "../src/collisionobject.c"
#endif // COLLISIONOBJECT_H

