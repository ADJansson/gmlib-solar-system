#ifndef PLANET_DATA_H
#define PLANET_DATA_H

#include <vector>

// Constants
// fra John Haugan s.172

#define SUN_MASS 1.99e30 // kg
#define SUN_RADIUS 6.96e8 // m
#define SUN_ROTATION 2160000 // s

#define MERCURY_MASS 3.3e23 // kg
#define MERCURY_ORBIT 5.79e10 // m
#define MERCURY_RADIUS 2.44e6 // m
#define MERCURY_VELOCITY 47362 // m/s
#define MERCURY_ROTATION 5097600 // s

#define VENUS_MASS 4.9e24
#define VENUS_ORBIT 1.08e11
#define VENUS_RADIUS 6.05e6
#define VENUS_VELOCITY 35000
#define VENUS_ROTATION 20995200

#define EARTH_MASS 6.0e24
#define EARTH_ORBIT 1.5e11
#define EARTH_RADIUS 6.38e6
#define EARTH_VELOCITY 29800
#define EARTH_ROTATION 86400
#define EARTH_TILT -24

#define MOON_MASS 7.35e22
#define MOON_FAKE_ORBIT 1.6e11
#define MOON_ORBIT 1.504e11
#define MOON_RADIUS 1.74e6
#define MOON_VELOCITY 30822
#define MOON_ROTATION 2373000

#define MARS_MASS 6.5e23
#define MARS_ORBIT 2.28e11
#define MARS_RADIUS 3.4e6
#define MARS_VELOCITY 24000
#define MARS_ROTATION 88560

#define JUPITER_MASS 1.9e27
#define JUPITER_ORBIT 7.78e11
#define JUPITER_RADIUS 7.14e7
#define JUPITER_VELOCITY 13000
#define JUPITER_ROTATION 35280

#define SATURN_MASS 5.7e26
#define SATURN_ORBIT 1.43e12
#define SATURN_RADIUS 6.0e7
#define SATURN_VELOCITY 9700
#define SATURN_ROTATION 36720
#define SATURN_TILT -30

#define URANUS_MASS 9.3e25
#define URANUS_ORBIT 2.87e12
#define URANUS_RADIUS 2.54e7
#define URANUS_VELOCITY 6800
#define URANUS_ROTATION 38880
#define URANUS_TILT -90

#define NEPTUNE_MASS 1.0e26
#define NEPTUNE_ORBIT 4.5e12
#define NEPTUNE_RADIUS 2.43e7
#define NEPTUNE_VELOCITY 5400
#define NEPTUNE_ROTATION 56880

#define KUIPER_MASS 1.0e24
#define KUIPER_ORBIT_MIN 5.7e12
#define KUIPER_ORBIT_MAX 6.3e12
#define KUIPER_RADIUS 1.5e6
#define KUIPER_VELOCITY 5000
#define KUIPER_ROTATION 552960

#define ASTEROID_MASS 4e15
#define ASTEROID_ORBIT_MIN 3.4e11
#define ASTEROID_ORBIT_MAX 5e11
#define ASTEROID_RADIUS 5e6
#define ASTEROID_VELOCITY 17000

#define SATELLITE_MASS 2000
#define SATELLITE_RADIUS 3
#define SATELLITE_VELOCITY 2400

#define STARSHIP_MASS 1e9

#define SPEED_OF_LIGHT 3e8

#define UNIVERSAL_GRAVITY_CONSTANT 6.674e-11


namespace ADJlib {

template <typename T>
class DataObject {

private:

    T _mass;
    float _orbit;
    float _radius;
    T _velocity;
    float _rotation;
    int _tilt;

    float _asteroid_orbit_min;
    float _asteroid_orbit_max;


public:

    DataObject(T mass, float orbit, float radius, T velocity, float rotation, int tilt, float asteroidOrbitMin = 0.f, float asteroidOrbitMax = 0.f);

    T getMass() const;
    float getOrbit() const;
    float getRadius() const;
    T getVelocity() const;
    float getRotation() const;
    int getTilt() const;

};


template <typename T>
class DataLoader {

public:

    std::vector<DataObject<T>> getPlanetDataVector();

};

}

#include "../src/planet_data.c"

#endif // PLANET_DATA_H


