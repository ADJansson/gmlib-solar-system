#include "../include/skybox.h"

namespace ADJlib {

Skybox::Skybox() {

    this->setSurroundingSphere(GMlib::Sphere<float, 3>(200.0f));
    auto skyboxViz = new SkyboxVisualizer();
    insertVisualizer(skyboxViz);
}
}
