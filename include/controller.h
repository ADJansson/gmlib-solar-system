#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "spaceobject.h"
#include "planet_data.h"
#include "collisionobject.h"
#include "starship.h"
#include "textureloader.h"
#include "model.h"

#include <vector>
#include <scene/gmsceneobject>
#include <gmParametricsModule>
#include <scene/gmpointlight>
#include <core/gmrandom>

class Scenario;

namespace ADJlib {

enum Mode {
    Game,
    Simulation,
};

#define MAX_DT_SCALING 5120000
#define MIN_DT_SCALING 312.5
#define GAME_DISTANCE_SCALING 7
#define SIMULATOR_DISTANCE_SCALING 9
#define GAME_SIZE_SCALING 3.8e-6
#define SIMULATOR_SIZE_SCALING 1e-6

template <typename T>
class Controller : public GMlib::SceneObject {
    GM_SCENEOBJECT(Controller)

    private:

        std::vector<std::shared_ptr<SpaceObject<T>>> _objects;
    std::vector<std::shared_ptr<SpaceObject<T>>> _despawnable_objects;
    std::shared_ptr<SpaceObject<T>> _starship;
    std::shared_ptr<Model> _jjModel;
    std::shared_ptr<GMlib::PSphere<float>> _camSphere;
    std::shared_ptr<GMlib::Camera> _cam;
    //std::shared_ptr<GMlib::PSphere<float>> _skybox;

    Scenario* _scenario;
    double _dt_sum;

    // Scaling
    double _grav_const;
    double _distance_scaling_pow;
    double _size_scaling;
    float _dt_multiplier;

    int _mode;

    // Starship controls
    int _yaw;
    float _thrust;
    int _pitch;
    int _roll;
    int _warp_factor;

    // Private helper functions
    void initGame();
    void initSimulation();
    GMlib::Vector<T, 3> computeForce(std::shared_ptr<SpaceObject<T>> o1, std::shared_ptr<SpaceObject<T>> o2) const;
    void collisionDetect(T dt);
    bool findCollision(std::shared_ptr<SpaceObject<T>> p1, std::shared_ptr<SpaceObject<T>> p2, T dt, T oldX, CollisionObject<T> &obj);
    void generateRandomBodies(int noOfObjects, float minOrbit, float maxOrbit, T mass, float radius, float velocity, GMlib::PSurfTexVisualizer<float, 3> *visualizer);
    void cleanUpDespawnableObjects();

    GMlib::Vector<T, 3> makeTVector(GMlib::Vector<float, 3> vector) const;
    GMlib::Vector<float, 3> makeFloatVector(GMlib::Vector<T, 3> vector) const;
    T scaleDownPhysics(T value) const;
    T scaleUpPhysics(T value) const;
    T scaleVisual(T size) const;

    void updateGuiScalingLabel() const;


public:

    Controller(Scenario *scenario, int mode);
    ~Controller();

    void makeSolarSystem(std::shared_ptr<GMlib::Camera> cam, GMlib::PointLight *light);

    // Simulation controls
    void increaseSimSpeed();
    void decreaseSimSpeed();

    // Starship controls
    void addThrustStarship(int direction);
    void yawStarship(int direction);
    void pitchStarship(int direction);
    void rollStarship(int direction);
    void engageWarp(int warpFactor);
    void fireTorpedo();

protected:
    void localSimulate(double dt);
};
}

#include "../src/controller.c"
#endif // CONTROLLER_H
