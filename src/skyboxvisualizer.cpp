#include "../include/skyboxvisualizer.h"

namespace ADJlib {

SkyboxVisualizer::SkyboxVisualizer() {

    loadSkyboxTextures();

    // Load shaders
    ShaderLoader sLoader;
    _prog = sLoader.loadShader("skybox");

    _color_prog.acquire("color");
    assert(_color_prog.isValid());

    // Vertex coordinates from learnopengl.com, modified to use
    // glm-vector
    _vertexArray = {

        glm::vec3(-1.0f,  1.0f, -1.0f),
        glm::vec3(-1.0f, -1.0f, -1.0f),
        glm::vec3( 1.0f, -1.0f, -1.0f),
        glm::vec3( 1.0f, -1.0f, -1.0f),
        glm::vec3( 1.0f,  1.0f, -1.0f),
        glm::vec3(-1.0f,  1.0f, -1.0f),

        glm::vec3(-1.0f, -1.0f,  1.0f),
        glm::vec3(-1.0f, -1.0f, -1.0f),
        glm::vec3(-1.0f,  1.0f, -1.0f),
        glm::vec3(-1.0f,  1.0f, -1.0f),
        glm::vec3(-1.0f,  1.0f,  1.0f),
        glm::vec3(-1.0f, -1.0f,  1.0f),

        glm::vec3( 1.0f, -1.0f, -1.0f),
        glm::vec3( 1.0f, -1.0f,  1.0f),
        glm::vec3( 1.0f,  1.0f,  1.0f),
        glm::vec3( 1.0f,  1.0f,  1.0f),
        glm::vec3( 1.0f,  1.0f, -1.0f),
        glm::vec3( 1.0f, -1.0f, -1.0f),

        glm::vec3(-1.0f, -1.0f,  1.0f),
        glm::vec3(-1.0f,  1.0f,  1.0f),
        glm::vec3( 1.0f,  1.0f,  1.0f),
        glm::vec3( 1.0f,  1.0f,  1.0f),
        glm::vec3( 1.0f, -1.0f,  1.0f),
        glm::vec3(-1.0f, -1.0f,  1.0f),

        glm::vec3(-1.0f,  1.0f, -1.0f),
        glm::vec3( 1.0f,  1.0f, -1.0f),
        glm::vec3( 1.0f,  1.0f,  1.0f),
        glm::vec3( 1.0f,  1.0f,  1.0f),
        glm::vec3(-1.0f,  1.0f,  1.0f),
        glm::vec3(-1.0f,  1.0f, -1.0f),

        glm::vec3(-1.0f, -1.0f, -1.0f),
        glm::vec3(-1.0f, -1.0f,  1.0f),
        glm::vec3( 1.0f, -1.0f, -1.0f),
        glm::vec3( 1.0f, -1.0f, -1.0f),
        glm::vec3(-1.0f, -1.0f,  1.0f),
        glm::vec3( 1.0f, -1.0f,  1.0f)
    };

    _vertexBuffer.create();
    _vertexBuffer.bufferData(_vertexArray.size() * sizeof(GMlib::GL::GLVertex), &_vertexArray[0], GL_STATIC_DRAW);
}

SkyboxVisualizer::SkyboxVisualizer(const SkyboxVisualizer &copy) {

}

void SkyboxVisualizer::render(const GMlib::SceneObject *obj, const GMlib::DefaultRenderer *renderer) const {

    const GMlib::Camera* cam = renderer->getCamera();
    const GMlib::HqMatrix<float, 3> &mvmat = obj->getModelViewMatrix(cam);

    GMlib::SqMatrix<float,3> rmat = mvmat.getRotationMatrix();

    this->glSetDisplayMode();

    _prog.bind(); {

        _prog.uniform( "u_rmat", rmat );

        // GMlib doesn't appear to support cube maps (yet anyway)
        glActiveTexture(GL_TEXTURE0);
        glUniform1i(glGetUniformLocation(_prog.getId(), "skybox_tex"), 0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, _texId);

        // Get vertex and texture attrib locations
        GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation("in_vertex");

        // Disable the depth test for skybox
        glDepthMask(GL_FALSE);
        _vertexBuffer.bind(); {
            _vertexBuffer.enable(vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertex), reinterpret_cast<const GLvoid *>(0x0) );
            _vertexBuffer.drawArrays(GL_TRIANGLES, 0, _vertexArray.size());
            _vertexBuffer.disable( vert_loc );
        }_vertexBuffer.unbind();
    } _prog.unbind();
    glDepthMask(GL_TRUE);
    glActiveTexture(GL_TEXTURE0);
    glDisable(GL_TEXTURE_CUBE_MAP); // Disable the cube map
}

void SkyboxVisualizer::renderGeometry(const GMlib::SceneObject *obj, const GMlib::Renderer *renderer, const GMlib::Color &color) const {

    // No geometry to render
}

/**
 * Load textures and create cube map
 */
void SkyboxVisualizer::loadSkyboxTextures() {

    QString baseFolderPath = "img\\skybox\\";

    std::vector<QString> skyboxImgs = {
        "front.jpg", "back.jpg", "left.jpg",
        "right.jpg", "top.jpg", "bottom.jpg"
    };

    std::cout << "Loading skybox textures " << std::endl;

    // GMlib doesn't appear to support cube maps
    // Therefore, we have to do it the good old gammelvei
    glGenTextures(1, &_texId);
    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_CUBE_MAP, _texId);

    for (int i = 0; i < skyboxImgs.size(); i++) {

        QString fileName = baseFolderPath + skyboxImgs[i];
        QImage img = QImage(fileName);

        if(!QFile::exists(fileName))
            std::cerr << "File " << fileName.toStdString() << " does not exist" << std::endl;

        if(img.isNull()) {
            std::cerr << "Image file is not supported!" << std::endl;
        }
        assert(!img.isNull());

        img = img.convertToFormat(QImage::Format_RGB888);

        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, img.width(), img.height(), 0, GL_RGB, GL_UNSIGNED_BYTE, img.bits());
    }

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}
}
