uniform sampler2D u_tex;
uniform vec4 u_color;

smooth in vec2 ex_tex;

out vec4 gl_FragColor;

void main() {

    gl_FragColor = texture2D(u_tex, ex_tex) * u_color;
}
