#include "../include/planet_data.h"

namespace ADJlib {

// BEGIN DataObject definitions

template <typename T>
DataObject<T>::DataObject(T mass, float orbit, float radius, T velocity, float rotation, int tilt, float asteroidOrbitMin, float asteroidOrbitMax) {

    _mass = mass;
    _orbit = orbit;
    _radius = radius;
    _velocity = velocity;
    _rotation = rotation;
    _tilt = tilt;

    _asteroid_orbit_min = asteroidOrbitMin;
    _asteroid_orbit_max = asteroidOrbitMax;
}


template <typename T>
T DataObject<T>::getMass() const {

    return _mass;
}


template <typename T>
float DataObject<T>::getOrbit() const {

    return _orbit;
}


template <typename T>
float DataObject<T>::getRadius() const {

    return _radius;
}


template <typename T>
T DataObject<T>::getVelocity() const {

    return _velocity;
}


template <typename T>
float DataObject<T>::getRotation() const {

    return _rotation;
}


template <typename T>
int DataObject<T>::getTilt() const {

    return _tilt;
}

// END DataObject definitions


// BEGIN DataLoader definitions
template <typename T>
std::vector<DataObject<T> > DataLoader<T>::getPlanetDataVector() {

    std::vector<DataObject<T>> dataObjects;

    dataObjects.push_back(DataObject<T>(SUN_MASS, 0, SUN_RADIUS, 0, SUN_ROTATION, 0));
    dataObjects.push_back(DataObject<T>(MERCURY_MASS, MERCURY_ORBIT, MERCURY_RADIUS, MERCURY_VELOCITY, MERCURY_ROTATION, 0));
    dataObjects.push_back(DataObject<T>(VENUS_MASS, VENUS_ORBIT, VENUS_RADIUS, VENUS_VELOCITY, VENUS_ROTATION, 0));
    dataObjects.push_back(DataObject<T>(EARTH_MASS, EARTH_ORBIT, EARTH_RADIUS, EARTH_VELOCITY, EARTH_ROTATION, EARTH_TILT));
    dataObjects.push_back(DataObject<T>(MOON_MASS, MOON_ORBIT, MOON_RADIUS, MOON_VELOCITY, MOON_ROTATION, 0));
    dataObjects.push_back(DataObject<T>(MARS_MASS, MARS_ORBIT, MARS_RADIUS, MARS_VELOCITY, MARS_ROTATION, 0));
    dataObjects.push_back(DataObject<T>(JUPITER_MASS, JUPITER_ORBIT, JUPITER_RADIUS, JUPITER_VELOCITY, JUPITER_ROTATION, 0));
    dataObjects.push_back(DataObject<T>(SATURN_MASS, SATURN_ORBIT, SATURN_RADIUS, SATURN_VELOCITY, SATURN_ROTATION, SATURN_TILT));
    dataObjects.push_back(DataObject<T>(URANUS_MASS, URANUS_ORBIT, URANUS_RADIUS, URANUS_VELOCITY, URANUS_ROTATION, URANUS_TILT));
    dataObjects.push_back(DataObject<T>(NEPTUNE_MASS, NEPTUNE_ORBIT, NEPTUNE_RADIUS, NEPTUNE_VELOCITY, NEPTUNE_ROTATION, 0));

    return dataObjects;
}

// END DataLoader definitions

} // END namespace ADJlib
