#ifndef STARSHIP
#define STARSHIP

#include "textureloader.h"

#include <memory>
#include <scene/gmsceneobject>
#include <parametrics/gmpcylinder>
#include <parametrics/gmpsphere>
#include <parametrics/gmpcone>
#include <parametrics/gmpplane>

namespace ADJlib {

class Starship : public GMlib::SceneObject {
    GM_SCENEOBJECT(Starship)

    private:

        std::vector<std::shared_ptr<GMlib::PSurf<float, 3>>> _parts;

public:

    Starship();
    ~Starship();

};
}

#endif // STARSHIP
