#ifndef MODELVISUALIZER_H
#define MODELVISUALIZER_H

#include "shaderloader.h"
#include "shape.h"

#include <gmParametricsModule>
#include <memory>

namespace ADJlib {

/*! \class ModelVisualizer
 *
 * Visulizer for model rendering. Uses shaders to display and textures of model.
 * Extends the built-in parametric surface visualizer of GMlib, with some
 * elements from the texture visualizer.
 *
 * Andreas Dyrøy Jansson 2016
 */
class ModelVisualizer : public GMlib::PSurfVisualizer<float, 3> {
    GM_VISUALIZER(ModelVisualizer)

public:
    ModelVisualizer();
    ModelVisualizer(const ModelVisualizer& copy);

    void setModelData(const std::vector<std::shared_ptr<Shape>> shapes);

    void render(const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer) const;
    void renderGeometry(const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color) const;

private:
    std::vector<std::shared_ptr<Shape>> _shapes;

    GMlib::GL::Program _prog;
    GMlib::GL::Program _color_prog;
};
}

#endif // MODELVISUALIZER_H
